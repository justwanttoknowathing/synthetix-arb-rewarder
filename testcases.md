# Tests by function

##  `function rewardCaller(uint bought, uint unspent_input) private returns (uint reward_tokens)`, `function expectedOutput(UniswapExchangeInterface exchange, uint input) private view returns (uint output)`, `function applySlippage(uint input) private view returns (uint output)`, `function maxConvert(uint a, uint b, uint n, uint d) private pure returns (uint result)`, `function sqrt(uint x) private pure returns (uint y)`, `function min(uint a, uint b) private pure returns (uint result)`
* Private functions. Not testable except by calling `addEth()`

## `function updateParams(uint _acceptable_slippage, uint _max_delay, uint _off_peg_min) external onlyOwner`

1. Not owner
* Should revert
2. Owner + out of bounds params (e.g. `_acceptable_slippage` >= `divisor`)
* Should revert
3. Owner + in bound params
* Should not revert
4. Post-non-reverting call
* `addEth()` should still function

## `function updateSnxAddr(address _snx_erc20_addr) external onlyOwner`
1. Not owner
* Should revert
2. Owner
* Should not revert
3. Post-non-reverting call
* `addEth()` should still function, provided contract has some of whatever token `snx_erc20_addr` now points to

## `function updateSethAddr(address _seth_erc20_addr, address _seth_exchange_addr) external onlyOwner`
1. Not owner
* Should revert
2. Owner
* Should not revert
3. Post-non-reverting call
* `addEth()` should still function, provided `seth_erc20_addr` is valid and `seth_exchange_addr` points to a populated uniswap exchange for that token


## `function updateRatesAddr(address _synxthetix_rates_addr) external onlyOwner`
1. Not owner
* Should revert
2. Owner
* Should not revert
3. Post-non-reverting call
* `addEth()` should still function, provided `synthetix_rates_addr` is a valid synthetix rates provider

## `function recoverETH(address payable to_addr) external onlyOwner`
* The contract should never hold ETH unless there is a bug. This function exists only as a failsafe.

## `recoverERC20(address erc20_addr, address to_addr) external onlyOwner`
1. Not owner
* Should revert
2. Owner
* Should not revert + should send specified ERC20 to address given, provided sufficient balance in-contract

## `function addEth() public payable rateNotStale("ETH") rateNotStale("SNX") returns (uint reward_tokens)`
### N.B. This function assumes that the synthetix rates provider contract _will never_ return 0 for `snx_rate` or `eth_rate`. This is probably safe, since those conditions seem very bad for the synthetix system as a whole.
1. ETH or SNX prices are not up to date according to synthetix rates provider
* Should revert
2. sETH/ETH Uniswap pool has a ETH/sETH ratio of `off_peg_min/divisor` (by default .99) or greater
* Maybe try: 99/100, 99.5/100, 100/100 and 1000/100
* Should revert
3. sETH/ETH uniswap exchange has 0 sETH or ETH (if one is 0, they must both be)
* Should revert
4. Contract holds 0 SNX
* Should revert
5. Contract has >0 SNX, but low number e.g. 100, uniswap exchange has very low ratio, e.g. 50 sETH / 100 ETH, sender provides 40 ETH
* Should revert provided in-contract SNX is insufficient to cover expected reward 
6. Uniswap exchange: 95 sETH, 100 ETH; Contract: 144k SNX, msg.value=0
* Should revert
7. Uniswap exchange: 95 sETH, 100 ETH; Contract: 144k SNX, msg.value=1
* Entire msg.value should be swapped
* 0 ETH returned to caller
* SNX returned to caller ~= (eth_rate * 1.03857) / snx_rate
* sETH in contract += ~1.03857
8. Uniswap exchange: 95 sETH, 100 ETH; Contract: 144k SNX, msg.value=10
* ~1.98235 eth should be swapped
* ~8.01765 eth should be returned to caller
* SNX returned to caller ~= (eth_rate * 2.03802) / snx_rate
* sETH in contract += ~2.03802
9. Pool: 10 sETH, 100 ETH; Contract: 144k SNX, msg.value=100
* ~21.49653 eth should be swapped
* ~78.50347 eth should be returned to caller
* SNX returned to caller ~= (eth_rate * 68.18533) / snx_rate
* sETH in contract += ~68.18533
10. `exmaples.py` can be used to come with any other scenarios that seem worthwhile
* Set `snx_rate`, `eth_rate`, `off_peg_min` and `divisor` in the script to match your expectations
* Give ETH in uniswap exchange, sETH in uniswap exchange and, optionally, an amount of ETH sent to the `addEth()` function
* E.g., to get numbers for the above test 7:
```
$ ./examples.py 95 100 1
 max_convert = 1.982353 ETH
 bought = 1.038574 sETH
 reward = 692.382748 SNX
 gain = 25.716081 SNX
```
