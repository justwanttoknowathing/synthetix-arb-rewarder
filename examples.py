#!/usr/bin/python3
 
import sys
from math import sqrt
 
snx_rate=.3
eth_rate=200
off_peg_min=9900
divisor=10000
 
try:
    eth_in_uniswap = float(sys.argv[1])
    seth_in_uniswap = float(sys.argv[2])
except:
    print("eth-in-pool seth-in-pool [msg_balance]")
    exit(1)

msg_balance = None
try:
	msg_balance = float(sys.argv[3])
except:
	pass
 
def mc(a,b,n,d):
    return (sqrt((a * (9*a*n + 3988000*b*d)) / n) - 1997*a) / 1994
 
def ex(c, f, t, b=997):
    return (b*c*f) / (1000*t + b*c);
 
if eth_in_uniswap/seth_in_uniswap < off_peg_min/divisor:
    max_convert = mc(eth_in_uniswap, seth_in_uniswap, divisor, off_peg_min)
    print(" max_convert = %f ETH" % max_convert)
    to_swap = max_convert
    if msg_balance is None:
        pass
    elif msg_balance > max_convert:
        print("You asked to swap %f, but this situation only allows a max of %f" % (msg_balance, max_convert))
    else:
        to_swap = msg_balance
    bought = ex(to_swap, seth_in_uniswap, eth_in_uniswap)
    print(" bought = %f sETH" % bought)
    reward = bought*eth_rate/snx_rate
    print(" reward = %f SNX" % reward)
    gain = reward - to_swap*eth_rate/snx_rate
    print(" gain = %f SNX" % gain)
else:
    print("bad ratio")
